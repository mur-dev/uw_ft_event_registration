/**
 * @file
 * Javascript for the event registration
 */
jQuery (document).submit(function () {
 	document.getElementById("submit_button").disabled = "disabled";
 	document.getElementById("submit_button").value = "Please Wait...";
});

jQuery(function() {
	$(document).ready(function() {
	  if ($('#edit-lead-type').val() == 'counsellor' || $('#edit-lead-type').val() == '') {
		jQuery('.form-item-start-year').hide();
	  }
	  else if ($('#edit-lead-type').val() == 'parent'){
		jQuery('.form-item-start-year').show();
		jQuery("label[for='edit-start-year']").text('When does your son/daughter plan to begin university?');
	  }
	  else {
		jQuery('.form-item-start-year').show();
		jQuery("label[for='edit-start-year']").text('When do you plan to begin university?');
	  }
	});
	jQuery('#edit-lead-type').change(function() {
	  if (this.value == 'counsellor' || this.value == '') {
		jQuery('.form-item-start-year').hide();
	  }
	  else if (this.value == 'parent'){
		jQuery('.form-item-start-year').show();
		jQuery("label[for='edit-start-year']").text('When does your son/daughter plan to begin university?');
	  }
	  else {
		jQuery('.form-item-start-year').show();
		jQuery("label[for='edit-start-year']").text('When do you plan to begin university?');
	  }
	});
});
