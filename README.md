# UW Event Registration Module
This module allow users to create registration forms. Their data is stored in the CRM. 
Usage: Any pages created as "Event Registration Type".

---
---
# Functions

---
---
### function uw_ft_event_registration_field_info()
Provides the description of the fields of the form.

##### Returns
An associative array whose keys define properties and values are information.

### function uw_ft_event_registration_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors)
This allows us to validate the form settings. *Drupal requires this function to exists.

---
---
### function uw_ft_event_registration_field_is_empty($items, $field)
Checks if a field is empty. This will raise an error if the field is required.

---
---
### function uw_ft_event_registration_field_formatter_info()
Declares that we will be using a custom view for the field, as a form.

##### Returns
An associative array with the default properties of a registration form.

---
---
### function uw_ft_event_registration_form($form, &$form_state, $item)
Develops the custom registration form for user input.

##### Returns
A nested associative array containing user inputted information.

##### Additional Information:
See the [api documentation](https://api.drupal.org/api/drupal/developer%21topics%21forms_api_reference.html/7.x) for form properties.

---
---
### function uw_ft_event_registration_session_list($form, &$form_state)
Rebuilds the form based on new selected location mode for the sessions.

##### Returns
An associative value for sessions in the form.

---
---
### function uw_ft_event_registration_session_validate($element, &$form_state) 
Validates the custom session dropdowns for the session form field.

---
---
### function uw_ft_event_registration_validate($form, &$form_state)
Validated the form inputs. This is will raise an error if a field is invalid.

---
---
### function uw_ft_event_registration_submit($form, &$form_state)
Creates a new EventRegistrant object and sets all the values.

---
---
### function uw_ft_event_registration_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display)
Adds custom form fields to the default form.

##### Returns
An array containing form fields and their properties.

---
---
### function uw_ft_event_registration_field_widget_info()
Declares the CKEditor as a widget for the forms.

##### Returns
An associative array containing edited information.

---
---
### function uw_ft_event_registration_field_presave($entity_type, $entity, $field, $instance, $langcode, &$items)
This fix the error with 'text-format' submitting as an array when expecting a string.

---
---
### function uw_ft_event_registration_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element)
Creates form elements for our field's widget. These are the fields that can be modified in CKEditor in Drupal.

##### Returns
A nested associative array containing fields for the widget.

---
---
### function uw_ft_event_registration_hidden_fields($element, &$form_state)
Handles serialization of hidden fields checkboxes.

---
---
### function uw_ft_event_registration_serialize($element, &$form_state)
Serializes session descriptions.

---
---
### function uw_ft_event_registration_field_widget_error($element, $error, $form, &$form_state)
Raises an error if a field in the widget is invalid.

---
---
### function uw_ft_event_registration_crm_authorization(&$session_id, &$url)
Authorization for the CRM.

---
---
### function uw_ft_event_registration_crm_call($method, $parameters, $url)
Makes a cURL request to the CRM.

##### Returns
The response of the server.

---
---
### function uw_ft_event_registration_add_lead_and_event_regsitration_crm($registrant)
Adds lead information to the CRM.

---
---
### function uw_ft_event_registration_get_lead($lead_id)
Get lead information based on id and if the lead already exists.

##### Returns
Lead information for the CRM.

---
---
### function uw_ft_event_registration_get_session_times($session_name)
Get session times based on session name.

##### Returns
An array of all the sessions.
